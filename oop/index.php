<?php
require('animal.php');

$sheep = new Animal("shaun");
echo "Name : ";
echo $sheep->name; // "shaun"
echo "<br>";
echo "legs : ";
echo $sheep->legs; // 4
echo "<br>";
echo "cold blooded : ";
echo $sheep->cold_blooded; // "no"
echo "<br><br>";

$kodok = new Frog("buduk");
echo "Name : ";
echo $kodok->name;
echo "<br>";
echo "legs : ";
echo $kodok->legs;
echo "<br>";
echo "cold blooded : ";
echo $kodok->cold_blooded;
echo "<br>";
echo "Jump : ";
$kodok->jump() ; // "hop hop"
echo "<br><br>";

$sungokong = new Ape("kera sakti");
echo "Name : ";
echo $sungokong->name;
echo "<br>";
echo "legs : ";
echo $sungokong->legs;
echo "<br>";
echo "cold blooded : ";
echo $sungokong->cold_blooded;
echo "<br>";
echo "Yell : ";
$sungokong->yell(); // "Auooo"